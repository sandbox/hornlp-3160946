<?php

namespace Drupal\drupal_path_guard\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RequestContext;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Routing\TrustedRedirectResponse;
Use Drupal\Core\Url;

/**
 * Class DrupalPathGuardEventSubscriber.
 *
 * @package Drupal\drupal_path_guard\DrupalPathGuardEventSubscriber
 */
class DrupalPathGuardEventSubscriber implements EventSubscriberInterface
{
    private $loggerFactory;
    private $uri_regex;
    private $context;
    private $controllerResolver;

    /**
     * @param \Symfony\Component\Routing\RequestContext $context
     *   Request context.    
     * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
     *   Logger Factory
     * @param \Drupal\Core\Controller\ControllerResolverInterface $controllerResolver
     *    ControllerResolver - to fix some WebProfiler issue when redirecting.
     */
    public function __construct(RequestContext $context, LoggerChannelFactoryInterface $loggerFactory, ControllerResolverInterface $controllerResolver)
    {
        $this->context = $context;
        $this->loggerFactory = $loggerFactory;
        $this->controllerResolver = $controllerResolver;

        $drupalpathguardsettings = \Drupal::config('drupal_path_guard.settings');
        $this->uri_regex = $drupalpathguardsettings->get('uri_regex');

        if (\Drupal::moduleHandler()->moduleExists('redirect')) {
            //$module = \Drupal::moduleHandler()->getModule('redirect');
        }
    }

    /**
     * React to a request being made, and filter for node_path_filter.
     * Block access to paths containing node_path_filter.
     *
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     *  The event to process.
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        try {
            // Get a clone of the request. During inbound processing the request
            // can be altered. Allowing this here can lead to unexpected behavior.
            // For example the path_processor.files inbound processor provided by
            // the system module alters both the path and the request; only the
            // changes to the request will be propagated, while the change to the
            // path will be lost.            
            $request = clone $event->getRequest();
            $uri = $request->getRequestUri();

            $this->context->fromRequest($request);

            $regex = $this->uri_regex;

            if (preg_match($regex, $uri)) {
                // only if user is guest ...
                $current_user = \Drupal::currentUser();
                if ($current_user->isAnonymous() === TRUE) {
                    $this->loggerFactory->get('default')
                        ->info('Drupal Path Guard stopped GUEST request to : ' . $uri);

                    // We want to redirect user to page specified in module.routing.yml.
                    $url = Url::fromRoute("drupal_path_guard.redirect_page", [])->toString();
                   
                   //dd($event);

                    // build new response
                    $headers = [
                        'X-Redirect-ID' => '0',
                      ];
                    $response = new TrustedRedirectResponse($url, 301, $headers);
                    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([])->addCacheTags(['rendered']));
                    $event->setResponse($response);    
                    //dd($event);                
                }
            }
        } catch (Exception $e) {
            // log exception
            $this->loggerFactory->get('error')
                ->error('Drupal Path Guard : ' . $e->message());
        }
    }


    /**
     * {@inheritdoc}
     *
     * @return array
     *   The event names to listen for, and the methods that should be executed.
     */
     public static function getSubscribedEvents()
     {
     // This needs to run before RouterListener::onKernelRequest(), which has
     // a priority of 32. Otherwise, that aborts the request if no matching
     // route is found.
     $events[KernelEvents::REQUEST][] = ['onKernelRequest', 33];
     return $events;        
         // return [
         //     KernelEvents::REQUEST => 'onKernelRequest',
         // ];
     }    
}

